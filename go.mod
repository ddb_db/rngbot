module github.com/slugger/rngbot

go 1.17

require (
	github.com/aws/aws-lambda-go v1.36.0
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.7.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/caarlos0/env/v6 v6.10.1
	github.com/go-resty/resty/v2 v2.7.0
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
