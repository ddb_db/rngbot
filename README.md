# RngBot: A Simple Interactive Mastodon Bot
This repo is a simple AWS Lambda function that acts as an interactive Mastodon bot.
This bot acts on SNS messages received from the [Mastodon Push Notification Gateway](https://gitlab.com/ddb_db/mstdnlambda)
and replies to them after parsing the request and generating a result.

What type of requests? In this small example, this bot listens for mentions and replies to them with
a random number or the result of flipping a coin -- when the message is properly crafted. Mentions that
are not formatted as expected are replied to with an error message.

## Flip a Coin
To have the bot flip a coin and reply with the result, send a mention as such:

`@rngbot@botsin.space flip a coin`

Upon receiving such a message, the bot will reply to the sender with the result of the coin flip.
To flip a coin, the message must be worded exactly as shown.

## Pick a Number
The other thing this bot will do is pick a random number within a given range of integers.

`@rngbot@botsin.space -2500 100000`

When such a message is received, the bot will reply with a number within the given range.
The formatting of these messages is a _little_ more forgiving.

`@rngbot@botsin.space random number between 0 and 100 please`

Such a message will work as expected.  Basically the bot parses the message looking for _exactly_
two integers in the message and as long as it finds exactly two integers, it will reply with 
a random number in the range.  If more or fewer than two numbers are found in the message then
you'll get an error response from the bot.

## Try it out
If you're on Mastodon, give the bot a try. Send it a message at `@rngbot@botsin.space`

## Why?
This is my first interactive Mastodon bot. This on its own has very little use (just don't tell the bot that!!)
but it was a very small, simple first project to get a feel for working with the various Mastodon APIs involved
in creating an interactive bot. I have other ideas in this area, this was just the starting point. I figured
since I created the bot and it works, I might as well post the source code as an example for other aspiring bot
creators.

## Tech Stack
Quick overview of the tech stack:

* Bot is written in Go
* Bot runs on AWS as a Lambda function
* Bot receives its messages via SNS subcription where the publisher is the AWS Mastodon Push Notification Gateway
  * Also written in Go
  * The gateway receives the push notifications from Mastodon, verifies them then sends the json data onto this lambda

It is worth noting that bots such as this do **not** need to be written in Go. You can deploy the gateway to verify and
route your notifications to any lambda, written in any language. Whatever language you choose, you just have to handle
income SNS messages in your code.
