package mstdn

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"

	"github.com/go-resty/resty/v2"
	"github.com/slugger/rngbot/internal/devenv"
	"github.com/slugger/rngbot/internal/logger"
)

type Client interface {
	Reply(notificationId int, result string) error
	ReplyError(notificationId int, err error) error
}

func NewClient(baseUrl string, oauthToken string) Client {
	if !devenv.IsActive() {
		clnt := resty.New().SetBaseURL(baseUrl).SetAuthScheme("Bearer").SetAuthToken(oauthToken).SetHeader("Content-Type", "application/json")
		return &defaultClient{
			baseUrl:    baseUrl,
			oauthToken: oauthToken,
			http:       clnt,
		}
	} else {
		return &devClient{}
	}
}

type post struct {
	Status         string `json:"status"`
	ReplyToId      string `json:"in_reply_to_id"`
	Visibility     string `json:"visibility"`
	idempotencyKey string
}

type status struct {
	Id         string `json:"id"`
	Visibility string `json:"visibility"`
}

type account struct {
	Username string `json:"acct"`
}

type notification struct {
	Id      string  `json:"id"`
	Account account `json:"account"`
	Status  status  `json:"status"`
}

type defaultClient struct {
	baseUrl    string
	oauthToken string
	http       *resty.Client
}

func (c *defaultClient) Reply(notificationId int, result string) error {
	return c.reply(notificationId, result)
}

func (c *defaultClient) ReplyError(notificationId int, err error) error {
	return c.reply(notificationId,
		fmt.Sprintf("I ran into a problem trying to answer your request.\n\nError: %s\n\nIf you can fix the error shown, please try again otherwise wait for my overlord to look into it.\n\n(All failures are automatically logged for the operator.)", err.Error()))
}

func (c *defaultClient) reply(nId int, reply string) error {
	n, err := c.fetchNotification(nId)
	if err != nil {
		return err
	}
	msg := fmt.Sprintf("@%s %s", n.Account.Username, reply)
	p := post{
		Status:         msg,
		ReplyToId:      n.Status.Id,
		Visibility:     n.Status.Visibility,
		idempotencyKey: hash(fmt.Sprintf("%s:%s", n.Status.Id, msg)),
	}
	_, err = c.http.R().SetBody(p).SetHeader("Idempotency-Key", p.idempotencyKey).Post("api/v1/statuses")
	return err
}

func (c *defaultClient) fetchNotification(id int) (*notification, error) {
	resp, err := c.http.R().SetResult(notification{}).Get(fmt.Sprintf("api/v1/notifications/%d", id))
	if err != nil {
		return nil, fmt.Errorf("[fetch notification failed] %w", err)
	}
	return resp.Result().(*notification), nil
}

type devClient struct{}

func (c *devClient) Reply(notificationId int, result string) error {
	logger.Log.WithField("notificationId", notificationId).WithField("result", result).Info("random number generated")
	return nil
}

func (c *devClient) ReplyError(notificationId int, err error) error {
	logger.Log.WithField("notificationId", notificationId).WithField("err", err).Error("request failed")
	return nil
}

func hash(input string) string {
	md5 := md5.New()
	return hex.EncodeToString(md5.Sum([]byte(input)))
}
