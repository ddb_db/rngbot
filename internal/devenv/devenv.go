package devenv

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strings"
)

type envMap map[string]string

func (e *envMap) String() string {
	vals := make([]string, len(*e))
	i := 0
	for k, v := range *e {
		vals[i] = fmt.Sprintf("[%s=%s]", k, v)
		i = i + 1
	}
	return strings.Join(vals, ",")
}

func (e *envMap) Set(val string) error {
	pair := strings.SplitN(val, "=", 2)
	if len(pair) != 2 {
		return fmt.Errorf("invalid env var: '%s'; expected var=val", val)
	}
	(*e)[pair[0]] = pair[1]
	return nil
}

var isDevEnv bool
var eventFile string
var env envMap

func init() {
	env = make(envMap)

	flag.StringVar(&eventFile, "event", "event.json", "SNS event input file")
	flag.BoolVar(&isDevEnv, "devenv", false, "must be set to true to trigger dev mode")
	flag.Var(&env, "env", "env var to set for lambda; can be used multiple times; format is var=val")
}

func IsActive() bool {
	return isDevEnv
}

func GetEvent(t interface{}) interface{} {
	data := getEventData()
	err := json.Unmarshal(data, t)
	if err != nil {
		panic(err)
	}
	return t
}

func getEventData() []byte {
	data, err := os.ReadFile(eventFile)
	if err != nil {
		panic(err)
	}
	return data
}

func InitArgs() {
	if !IsActive() {
		return
	}

	flag.Visit(func(f *flag.Flag) {
		if f.Name != "env" {
			return
		}
		for k, v := range env {
			if err := os.Setenv(k, v); err != nil {
				panic(err)
			}
		}
	})
}
