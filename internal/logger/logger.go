package logger

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/slugger/rngbot/internal/cfg"
)

var Log *logrus.Entry

func Reset() {
	logLvl, err := logrus.ParseLevel(cfg.Cfg.LogLevel())
	if err != nil {
		logLvl = logrus.InfoLevel
	}
	l := logrus.Logger{
		Out:       os.Stdout,
		Formatter: &logrus.JSONFormatter{},
		Level:     logLvl,
	}
	Log = logrus.NewEntry(&l)
}
