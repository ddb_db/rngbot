package cfg

import "github.com/caarlos0/env/v6"

type Config interface {
	LogLevel() string
	ApiUrl() string
	ApiKey() string
}

var Cfg Config

func init() {
	Cfg = &configSettings{}
}

func NewConfig() Config {
	return Cfg
}

func ParseConfig() Config {
	cfg := configSettings{}
	if err := env.Parse(&cfg); err != nil {
		panic(err)
	}
	Cfg = &cfg
	return Cfg
}

type configSettings struct {
	ApiKeyValue   string `env:"RNGBOT_API_KEY,notEmpty,unset"`
	ApiUrlValue   string `env:"RNGBOT_API_URL,notEmpty"`
	LogLevelValue string `env:"RNGBOT_LOG_LEVEL" envDefault:"INFO"`
}

func (c *configSettings) ApiKey() string   { return c.ApiKeyValue }
func (c *configSettings) ApiUrl() string   { return c.ApiUrlValue }
func (c *configSettings) LogLevel() string { return c.LogLevelValue }
