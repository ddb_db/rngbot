package rng

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

type Notification struct {
	Id     int    `json:"notification_id"`
	Type   string `json:"notification_type"`
	Body   string `json:"body"`
	Token  string `json:"access_token"`
	Locale string `json:"preferred_locale"`
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func Process(n *Notification) (string, error) {
	if strings.HasSuffix(strings.TrimSpace(strings.ToLower(n.Body)), "flip a coin") {
		result := rand.Intn(2)
		if result == 0 {
			return "tails", nil
		}
		return "heads", nil
	}
	return parse(n)
}

func parse(n *Notification) (string, error) {
	args := strings.Split(n.Body, " ")
	intsFound := 0
	if len(args) > 2 {
		inputs := make([]int, 2)
		for _, t := range args {
			if t == "" {
				continue
			}
			if i, err := strconv.Atoi(strings.Trim(strings.TrimSpace(t), ".,;*#@!^()")); err == nil {
				intsFound = intsFound + 1
				if intsFound > 2 {
					return "", fmt.Errorf("too many numbers")
				}
				inputs[intsFound-1] = i
			}
		}
		if intsFound != 2 {
			return "", fmt.Errorf("need two numbers")
		}

		var n, m int
		if inputs[0] == inputs[1] {
			return fmt.Sprintf("%d", inputs[0]), nil
		} else if inputs[0] > inputs[1] {
			n = inputs[1]
			m = inputs[0]
		} else {
			n = inputs[0]
			m = inputs[1]
		}
		m = m + 1 // closed interval for rand.Intn
		result := rand.Intn(m-n) + n
		return fmt.Sprintf("%d", result), nil
	} else {
		return "", fmt.Errorf("message is too short")
	}
}
