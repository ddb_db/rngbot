package rng_test

import (
	"strconv"
	"testing"

	"github.com/slugger/rngbot/internal/rng"
	"github.com/stretchr/testify/assert"
)

func TestProcessSucceedsWithValidInputNumbers(t *testing.T) {
	testCases := []struct {
		msg      string
		min, max int
		desc     string
	}{
		{"@user 0 100", 0, 100, "two ints, a<b"},
		{"@user 100 0", 0, 100, "two ints, a>b"},
		{"@user -5 -5", -5, -5, "two ints a==b"},
		{"@user -1000, 10000!", -1000, 10000, "two ints, one negative, trailing puncuation, a<b"},
		{"@user beween 20 and 5  ", 5, 20, "two ints with words, b<a"},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			r, err := rng.Process(buildNotification(tc.msg))
			assert.Nil(t, err)
			n, err := strconv.Atoi(r)
			assert.Nil(t, err)
			assert.LessOrEqual(t, n, tc.max)
			assert.GreaterOrEqual(t, n, tc.min)
		})
	}
}

func TestReturnsErrorWhenMessageHasTooFewNumbers(t *testing.T) {
	n := buildNotification("@user need a random between one and -6")
	_, err := rng.Process(n)
	assert.ErrorContains(t, err, "need two numbers")
}

func TestReturnsErrorWhenMessageHasTooManyNumbers(t *testing.T) {
	n := buildNotification("@user 0   1   2  ")
	_, err := rng.Process(n)
	assert.ErrorContains(t, err, "too many numbers")
}

func TestReturnsErrorWhenMessageIsNotLongEnough(t *testing.T) {
	n := buildNotification("@user Hi!")
	_, err := rng.Process(n)
	assert.ErrorContains(t, err, "message is too short")
}

func TestResultIsHeadsOrTailsWhenAskedToFlipCoin(t *testing.T) {
	testCases := []struct {
		msg string
	}{
		{"@user flip a coin"},
		{"@user flip a coin "},
		{"@user please flip a coin"},
		{"@user FLip A coIn"},
	}

	for _, tc := range testCases {
		t.Run(tc.msg, func(t *testing.T) {
			result, err := rng.Process(buildNotification(tc.msg))
			assert.Nil(t, err)
			assert.True(t, result == "heads" || result == "tails")
		})
	}
}

func buildNotification(msg string) *rng.Notification {
	return &rng.Notification{
		Body: msg,
	}
}
