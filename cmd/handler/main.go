package main

import (
	"encoding/json"
	"flag"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/slugger/rngbot/internal/cfg"
	"github.com/slugger/rngbot/internal/devenv"
	"github.com/slugger/rngbot/internal/logger"
	"github.com/slugger/rngbot/internal/mstdn"
	"github.com/slugger/rngbot/internal/rng"
)

func main() {
	flag.Parse()
	devenv.InitArgs()
	cfg.ParseConfig()
	logger.Reset()

	if !devenv.IsActive() {
		lambda.Start(handler)
	} else {
		e := events.SNSEvent{}
		if err := handler(*(devenv.GetEvent(&e).(*events.SNSEvent))); err != nil {
			panic(err)
		}
	}
}

func handler(event events.SNSEvent) error {
	clnt := mstdn.NewClient(cfg.Cfg.ApiUrl(), cfg.Cfg.ApiKey())
	for _, r := range event.Records {
		input := rng.Notification{}
		if err := json.Unmarshal([]byte(r.SNS.Message), &input); err != nil {
			return err
		}
		n, err := rng.Process(&input)
		if err != nil {
			if e := clnt.ReplyError(input.Id, err); e != nil {
				return e
			}
		} else {
			if e := clnt.Reply(input.Id, n); e != nil {
				return e
			}
		}
	}
	return nil
}
